import { Pool } from 'pg';

const pool = new Pool({
	connectionString: process.env.DATABASE_URL
});

export const query = (sql, params) => pool.query(sql, params);
