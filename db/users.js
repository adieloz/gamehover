import { query } from './pg';

export const getAll = async () => {
	const sql = `
        SELECT * 
        FROM users
    `;

	const { rows } = await query(sql);
	return rows;
};

export const getById = async id => {
	const sql = `
        SELECT * 
        FROM users
        WHERE id = $1
    `;

	const {
		rows: [user]
	} = await query(sql, [id]);

	return user;
};
