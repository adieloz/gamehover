import express from 'express';
import games from './games';
import users from './users';

const router = express.Router();

router.use((req, res, next) => {
	if (req.session.email) {
		return next();
	}

	res.status(500).json({ not: 'auth' });
});

router.use('/games', games);
router.use('/users', users);

export default router;
