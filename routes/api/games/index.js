import express from 'express';

const router = express.Router();

router.get('/', (req, res) => {
	res.send('this is games get');
});

router.get('/:id', (req, res) => {
	res.send(`this is game ${req.params.id}`);
});

export default router;
