import express from 'express';
import { getAll, getById } from '@gh/db/users';

const router = express.Router();

router.get('/', async (req, res) => {
	try {
		const users = await getAll();
		res.json(users);
	} catch (ex) {
		res.status(500).send('error');
	}
});

router.get('/:id', async (req, res) => {
	try {
		const user = await getById(req.params.id);
		res.json(user);
	} catch (ex) {
		res.status(500).send('error');
	}
});

export default router;
