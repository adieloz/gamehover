import path from 'path';
import express from 'express';
import api from './api';
import auth from './auth';

const router = express.Router();

router.use('/api', api);
router.use('/auth', auth);

router.get('/*', (req, res) => {
	res.sendFile(path.resolve(__dirname, '..', 'dist', 'index.html'));
});

export default router;
