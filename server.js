import express from 'express';
import open from 'open';
import session from 'express-session';
import serverDev from '@gh/server.dev';
import routes from './routes';

const app = express();
const port = process.env.PORT || 9000;
const appURL = `http://localhost:${port}`;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(
	session({
		secret: 'cs',
		resave: false,
		saveUninitialized: true
	})
);

// app.use(express.static('public'));

serverDev(app);

app.use(routes);

app.listen(port, () => {
	console.log(`Server started ${appURL}`);
	//open(appURL);
});
