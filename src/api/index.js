import axios from 'axios';

export const getAllUsers = () => {
	return axios.get('/api/users');
};

export const getById = id => {
	return axios.get(`/api/users/${id}`);
};

export const login = (email, password) => {
	return axios({
		method: 'post',
		url: '/auth/login',
		data: {
			email,
			password
		}
	});
};
