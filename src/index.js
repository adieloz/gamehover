import React from 'react';
import ReactDOM from 'react-dom';
import Root from './components/root';
import { getRootElement } from '@ghc/utils/dom';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(<Root />, getRootElement());

if (module.hot) {
	module.hot.accept();
}
