import React, { useState } from 'react';
import { BrowserRouter, Switch, Route, Link, Redirect } from 'react-router-dom';
import App from './app';
import Login from './login';

const Root = () => {
	const [auth, setAuth] = useState(false);

	return (
		<BrowserRouter>
			<Switch>
				<Route
					path="/admin"
					render={props => {
						if (auth) {
							return <App />;
						} else {
							return <Redirect to="/login" />;
						}
					}}
				></Route>
				<Route path="/">
					<Login setAuth={setAuth} />
				</Route>
			</Switch>
		</BrowserRouter>
	);
};

export default Root;
