import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import Ajax from './ajax';

const App = () => {
	const history = useHistory();

	return (
		<>
			<Button
				onClick={e => {
					history.push('/login');
				}}
			>
				Go To Login
			</Button>
			<Button
				onClick={e => {
					history.push('/login2');
				}}
			>
				Go To Login2
			</Button>
			<Ajax />
		</>
	);
};

export default App;
