import React, { useEffect, useState } from 'react';
import { Button, Alert } from 'react-bootstrap';
import { getAllUsers, getById } from '@ghc/api';

const Ajax = () => {
	const [users, setUsers] = useState([]);
	const [val, setVal] = useState('');

	useEffect(() => {
		(async () => {
			const { data: users } = await getAllUsers();
			setUsers(users);
		})();
		// here i will load data from the ajax when the component loads
	}, []);

	return (
		<>
			<Button
				variant="primary"
				onClick={async () => {
					const { data: user } = await getById(val);
					console.log(user);
				}}
			>
				Go Tos Server
			</Button>
			<input value={val} onChange={e => setVal(e.target.value)} />
			{users.length ? null : <div>Loading...</div>}
			{users.map(user => (
				<Alert variant="dark" key={user.id}>{`${user.name}`}</Alert>
			))}
		</>
	);
};

export default Ajax;
