import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import { login } from '@ghc/api';

// eslint-disable-next-line react/prop-types
const Login = ({ setAuth }) => {
	const history = useHistory('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	return (
		<Form>
			<Form.Group controlId="formBasicEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					Well never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="formBasicPassword">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>
			<Button
				variant="primary"
				type="submit"
				onClick={async e => {
					e.preventDefault();
					const { data } = await login(email, password);
					if (data.success) {
						setAuth(true);
						return history.push('/admin');
					}

					console.log('Error');
				}}
			>
				Submit
			</Button>
		</Form>
	);
};

export default Login;
