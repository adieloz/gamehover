const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');

module.exports = {
	mode: 'development',
	devtool: 'source-map',
	entry: ['webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000', './src'],
	output: {
		publicPath: '/',
		filename: 'dist/app.js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: ['babel-loader', 'eslint-loader']
			},
			{
				test: /\.(sa|sc|c)ss$/,
				use: [
					'style-loader',
					{
						loader: 'css-loader',
						options: {
							sourceMap: true
						}
					},
					{
						loader: 'sass-loader',
						options: {
							sourceMap: true
						}
					}
				]
			}
		]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify('development')
		}),
		new HtmlWebpackPlugin({
			alwaysWriteToDisk: true,
			title: 'Game Hover',
			meta: { viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no' }
		}),
		new HtmlWebpackHarddiskPlugin()
	]
};
